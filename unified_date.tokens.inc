<?php

/**
 * @file
 * Builds replacement tokens for the unified_date field.
 */

use Drupal\Core\Datetime\Entity\DateFormat;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Render\BubbleableMetadata;

/**
 * Implements hook_token_info_alter().
 */
function unified_date_token_info_alter(&$data): void {
  $data['tokens']['node']['unified_date'] = [
    'name' => t('Unified Date'),
    'description' => t('The unified date field.'),
    'type' => 'date',
  ];
}

/**
 * Implements hook_tokens().
 */
function unified_date_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata): array {
  $token_service = \Drupal::token();
  $replacements = [];
  if ($type === 'node' && !empty($data['node'])) {
    /** @var \Drupal\node\NodeInterface $node */
    $node = $data['node'];
    if (isset($tokens['unified_date'])) {
      $language_code = LanguageInterface::LANGCODE_DEFAULT;
      if (isset($options['langcode'])) {
        $language_code = $options['langcode'];
      }
      $original = $tokens['unified_date'];
      $date_format = DateFormat::load('medium');
      $bubbleable_metadata->addCacheableDependency($date_format);
      $replacements[$original] = \Drupal::service('date.formatter')->format($node->get('unified_date')->value, 'medium', '', NULL, $language_code);
    }

    if ($created_tokens = $token_service->findWithPrefix($tokens, 'unified_date')) {
      $replacements += $token_service->generate('date', $created_tokens, ['date' => $node->get('unified_date')->value], $options, $bubbleable_metadata);
    }
  }

  return $replacements;
}
