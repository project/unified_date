<?php

namespace Drupal\unified_date;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\Entity\BaseFieldOverride;
use Drupal\field\FieldConfigInterface;
use Drupal\node\NodeInterface;

/**
 * Unified date manager.
 */
class UnifiedDateManager {

  /**
   * Raw unified date settings config.
   *
   * @var array
   */
  protected array $configRawData = [];

  /**
   * UnifiedDateManager constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager
   *   The entity field manager.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler.
   */
  public function __construct(
    protected ConfigFactoryInterface $configFactory,
    protected EntityFieldManagerInterface $entityFieldManager,
    protected ModuleHandlerInterface $moduleHandler,
  ) {
    $this->configRawData = $this->configFactory->get('unified_date.settings')->getRawData();
  }

  /**
   * Get node unified date.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The node object.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function getUnifiedDate(NodeInterface $node) {

    $node_type = $node->getType();

    // Reload the configuration if in a test environment.
    // This ensures that we can more efficiently test.
    if (function_exists('drupal_valid_test_ua') && drupal_valid_test_ua()) {
      $this->configRawData = $this->configFactory->get('unified_date.settings')->getRawData();
    }

    // Field where date is stored.
    $field = 'base-field:created';
    if (isset($this->configRawData['node_types'][$node_type])) {
      $field = $this->configRawData['node_types'][$node_type];
    }

    // Strip base-field prefix.
    $field = str_replace('base-field:', '', $field);

    // Value location may be different than value.
    // Eg, `field_datetime_range:end_value`.
    $value_location = 'value';
    if (str_contains($field, ':')) {
      $field_parts = explode(':', $field);
      $field = $field_parts[0];
      $value_location = $field_parts[1];
    }

    // Determine date.
    $date = NULL;
    if ($node->hasField($field)) {
      $date = $node->get($field)->{$value_location};
      if (!is_numeric($date) && !is_null($date)) {
        $date = strtotime($date);
      }
    }

    // Set Default to created time if unified date is empty.
    if (!$date) {
      $date = $node->getCreatedTime();
    }

    // Let other modules to alter date value.
    $this->moduleHandler->alter('unified_date', $date, $node);

    return $date;
  }

  /**
   * Set node unified date.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The node object.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function setNodeUnifiedDate(NodeInterface $node) {
    $node->unified_date = $this->getUnifiedDate($node);
    $node->save();
  }

  /**
   * Get node fields with date type.
   *
   * @param string $node_type
   *   The node type.
   *
   * @return mixed
   *   The array of date field keys.
   */
  public function getNodeDateFields($node_type) {
    $date_fields = [];

    // Get all field attached to a particular node type.
    $field_definitions = $this->entityFieldManager->getFieldDefinitions('node', $node_type);
    foreach ($field_definitions as $field_definition) {
      // Standard fields.
      $standard_fields = ['datetime', 'timestamp'];
      if (
        $field_definition instanceof FieldConfigInterface
        && in_array($field_definition->getType(), $standard_fields)
      ) {
        $date_fields[] = $field_definition->getName();
      }

      // Date range fields.
      $daterange_fields = ['daterange', 'daterange_timezone', 'smartdate'];
      if (
        $field_definition instanceof FieldConfigInterface
        && in_array($field_definition->getType(), $daterange_fields)
      ) {
        $date_fields[] = $field_definition->getName();
        $date_fields[] = $field_definition->getName() . ':end_value';
      }

      // For entity base fields.
      if ($field_definition instanceof BaseFieldDefinition || $field_definition instanceof BaseFieldOverride) {
        if (in_array($field_definition->getName(), [
          'created',
          'changed',
          'published_at',
        ])) {
          $date_fields[] = 'base-field:' . $field_definition->getName();
        }
      }
    }
    return array_combine($date_fields, $date_fields);
  }

}
