<?php

namespace Drupal\unified_date\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\unified_date\UnifiedDateManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure menu link weight settings.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * Constructs a new bulk update form.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\unified_date\UnifiedDateManager $unifiedDateManager
   *   The unified date manager.
   */
  public function __construct(
    protected EntityTypeManagerInterface $entityTypeManager,
    protected UnifiedDateManager $unifiedDateManager,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('unified_date.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'unified_date_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['unified_date.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('unified_date.settings');

    $node_types = $this->entityTypeManager
      ->getStorage('node_type')
      ->loadMultiple();
    if ($node_types) {
      foreach (array_keys($node_types) as $node_type) {
        $form[$node_type] = [
          '#type'     => 'select',
          '#required' => TRUE,
          '#options'  => $this->unifiedDateManager->getNodeDateFields($node_type),
          '#title'    => $this->t('Node type "@node_type"', [
            '@node_type' => $node_type,
          ]),
          '#description' => $this->t('All node types without date fields selected will used the "Created" date.'),
        ];
        if ($config && $default_value = $config->get('node_types.' . $node_type)) {
          $form[$node_type]['#default_value'] = $default_value;
        }
      }
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('unified_date.settings');

    // Loop through node types and store field selection in config.
    $node_types = $this->entityTypeManager
      ->getStorage('node_type')
      ->loadMultiple();
    if ($node_types) {
      foreach (array_keys($node_types) as $node_type) {
        $config->set('node_types.' . $node_type, $form_state->getValue($node_type));
      }
    }

    // Save the config.
    $config->save();

    parent::submitForm($form, $form_state);
  }

}
