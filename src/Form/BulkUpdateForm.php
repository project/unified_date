<?php

namespace Drupal\unified_date\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure file system settings for this site.
 */
class BulkUpdateForm extends FormBase {

  /**
   * Constructs a new bulk update form.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(
    protected EntityTypeManagerInterface $entityTypeManager,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'unified_date_bulk_update_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form = [];

    $form['#update_callbacks'] = [];

    $form['update'] = [
      '#type'          => 'checkboxes',
      '#title'         => $this->t('Select the node types of paths for which to set the unified date'),
      '#options'       => [],
      '#default_value' => [],
    ];

    $node_types = $this->entityTypeManager
      ->getStorage('node_type')
      ->loadMultiple();
    if ($node_types) {
      $options = [];

      // Get all node types.
      foreach ($node_types as $node_type => $node_definition) {
        $options[$node_type] = $node_definition->label();
      }

      $form['node_types'] = [
        '#type'          => 'checkboxes',
        '#options'       => $options,
        '#default_value' => array_keys($options),
        '#title'         => $this->t('Node types'),
        '#required'      => TRUE,
      ];
    }

    $form['actions']['#type']  = 'actions';
    $form['actions']['submit'] = [
      '#type'  => 'submit',
      '#value' => $this->t('Update Unified Dates'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $settings = $form_state->getValues();
    $batch = [
      'title' => $this->t('Bulk updating Unified Dates'),
      'operations' => [],
      'finished' => 'Drupal\unified_date\UnifiedDateBatchProcessor::finishedBatch',
    ];
    $batch['operations'][] = [
      'Drupal\unified_date\UnifiedDateBatchProcessor::processBatch',
      [$settings],
    ];
    batch_set($batch);
  }

}
