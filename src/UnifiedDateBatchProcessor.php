<?php

namespace Drupal\unified_date;

use Drupal\node\Entity\Node;

/**
 * Unified date batch processor.
 */
class UnifiedDateBatchProcessor {

  /**
   * Process callback for the batch set BulkUpdateForm.
   *
   * @param array $settings
   *   The settings from the export form.
   * @param array $context
   *   The batch context.
   */
  public static function processBatch(array $settings, array &$context) {
    if (empty($context['sandbox'])) {

      // Store data in results for batch finish.
      $context['results']['data'] = [];
      $context['results']['settings'] = $settings;
      $context['results']['updates'] = 0;

      // Set initial batch progress.
      $context['sandbox']['settings'] = $settings;
      $context['sandbox']['progress'] = 0;
      $context['sandbox']['current_id'] = 0;
      $context['sandbox']['max'] = self::getMax($settings);

    }
    else {
      $settings = $context['sandbox']['settings'];
    }

    $context['sandbox']['max'] = $context['sandbox']['max'] ?? 0;
    if ($context['sandbox']['max'] == 0) {

      // If we have no rows to export, immediately finish.
      $context['finished'] = 1;

    }
    else {

      // Get the next batch worth of data.
      self::setNodeDates($settings, $context);

      // Check if we are now finished.
      if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
        $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
      }

    }

  }

  /**
   * Get the submissions for the given contact form.
   *
   * @param array $settings
   *   The settings from the export form.
   * @param array $context
   *   The batch context.
   */
  protected static function setNodeDates(array $settings, array &$context) {
    $limit = 5;
    $nids  = \Drupal::entityQuery('node')
      ->accessCheck(FALSE)
      ->condition('nid', $context['sandbox']['current_id'], '>')
      ->condition('type', $settings['node_types'], 'IN')
      ->range(0, $limit)
      ->sort('nid', 'ASC')
      ->execute();
    if ($nids) {
      /** @var \Drupal\unified_date\UnifiedDateManager $unified_date_manager */
      $unified_date_manager = \Drupal::service('unified_date.manager');
      if ($nodes = Node::loadMultiple($nids)) {
        foreach ($nodes as $node) {
          $unified_date_manager->setNodeUnifiedDate($node);
          $context['sandbox']['current_id'] = $node->id();
          $context['sandbox']['progress']++;
          $context['results']['updates']++;
        }
      }
    }
  }

  /**
   * Get max amount of nodes to update.
   *
   * @param array $settings
   *   The settings from the export form.
   *
   * @return int
   *   The maximum number of messages to export.
   */
  protected static function getMax(array $settings) {
    $result = \Drupal::entityQuery('node')
      ->accessCheck(FALSE)
      ->condition('type', $settings['node_types'], 'IN')
      ->count()
      ->execute();
    return ($result ? $result : 0);
  }

  /**
   * Batch finished callback.
   */
  public static function finishedBatch($success, $results, $operations) {
    if ($success) {
      if (isset($results['updates']) && $results['updates']) {
        $message = \Drupal::translation()->formatPlural(
          $results['updates'],
          'Generated 1 Unified Date.',
          'Generated @count Unified Dates.',
        );
        \Drupal::service('messenger')->addMessage($message);
      }
      else {
        $message = t('No Unified Dates to generate.');
        \Drupal::service('messenger')->addMessage($message);
      }
    }
    else {
      $message = t('An error occurred while processing the Unified Dates with arguments : @args', [
        '@args' => reset($operations),
      ]);
      \Drupal::service('messenger')->addMessage($message);
    }
  }

}
