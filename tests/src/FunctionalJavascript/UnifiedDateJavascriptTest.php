<?php

namespace Drupal\Tests\unified_date\FunctionalJavascript;

use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\node\Entity\Node;

/**
 * Tests Book javascript functionality.
 *
 * @group book
 */
class UnifiedDateJavascriptTest extends WebDriverTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'unified_date',
    'unified_date_test_config',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Tests re-ordering of books.
   */
  public function testUnifiedDateSettingsChange() {
    $current_time = time();
    $two_days_ago = strtotime('-2 days');
    $publication1 = Node::create([
      'type' => 'publication',
      'title' => 'Publication 1',
      'created' => $current_time,
      'field_publication_date' => date('Y-m-d', $two_days_ago),
    ]);
    $publication1->save();

    // Head to admin screen and attempt to re-order.
    $this->drupalLogin($this->drupalCreateUser([
      'administer site configuration',
      'bypass node access',
      'administer nodes',
    ]));
    $this->drupalGet('admin/config/content/unified-date');

    $this->submitForm([
      'publication' => 'field_publication_date',
    ], 'Save configuration');
    $this->assertSession()->pageTextContains('The configuration options have been saved.');
    $this->assertSession()->fieldValueEquals('publication', 'field_publication_date');

    // Date originally set as created date.
    $current_date = date('Y-m-d', $current_time);
    $two_days_ago_date = date('Y-m-d', $two_days_ago);
    $unified_date_date = date('Y-m-d', $publication1->unified_date->value);
    $this->assertSame($unified_date_date, $current_date, 'The unified date is the same as the current date on the base field');

    // Save the node on the regular node edit form.
    $this->drupalGet('node/' . $publication1->id() . '/edit');
    $this->submitForm([], 'Save');

    // Reload the node.
    $storage = $this->container->get('entity_type.manager')->getStorage('node');
    $storage->resetCache([1]);
    $publication1 = $storage->load($publication1->id());

    // Now that we changed the config, check that the date
    // is now the publication date.
    $unified_date_date = date('Y-m-d', $publication1->unified_date->value);
    $this->assertSame($unified_date_date, $two_days_ago_date, 'The unified date is the same as the two days ago date on the publication date field');
  }

}
