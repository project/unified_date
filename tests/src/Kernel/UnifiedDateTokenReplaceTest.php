<?php

declare(strict_types=1);

namespace Drupal\Tests\node\Kernel;

use Drupal\Core\Datetime\Entity\DateFormat;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\node\Entity\Node;
use Drupal\Tests\system\Kernel\Token\TokenReplaceKernelTestBase;

/**
 * Tests node token replacement.
 *
 * @group node
 */
class UnifiedDateTokenReplaceTest extends TokenReplaceKernelTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'node',
    'datetime',
    'unified_date',
    'unified_date_test_config',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installConfig(['filter', 'node', 'datetime']);

    $this->installSchema('node', ['node_access']);
    $this->installEntitySchema('node');
  }

  /**
   * Creates a node, then tests the unified date generated from it.
   */
  public function testUnifiedDateTokenReplacement() {
    /** @var \Drupal\node\NodeInterface $node */
    $node = Node::create([
      'type' => 'publication',
      'title' => 'Test publication',
    ]);
    $node->save();

    // Generate and test tokens.
    $tests = [];

    /** @var \Drupal\Core\Datetime\DateFormatterInterface $date_formatter */
    $date_formatter = $this->container->get('date.formatter');
    $tests['[node:unified_date:since]'] = $date_formatter->formatTimeDiffSince($node->getCreatedTime(), ['langcode' => $this->interfaceLanguage->getId()]);

    $base_bubbleable_metadata = BubbleableMetadata::createFromObject($node);
    $metadata_tests = [];
    $bubbleable_metadata = clone $base_bubbleable_metadata;
    $metadata_tests['[node:unified_date:since]'] = $bubbleable_metadata->setCacheMaxAge(0);

    // Test to make sure that we generated something for each token.
    $this->assertNotContains(0, array_map('strlen', $tests), 'No empty tokens generated.');

    foreach ($tests as $input => $expected) {
      $bubbleable_metadata = new BubbleableMetadata();
      $output = $this->tokenService->replace($input, ['node' => $node], ['langcode' => $this->interfaceLanguage->getId()], $bubbleable_metadata);
      $this->assertSame((string) $expected, $output, "Failed test case: {$input}");
      $this->assertEquals($metadata_tests[$input], $bubbleable_metadata);
    }

    // Change to use created date.
    $config_factory = \Drupal::configFactory();
    $config_factory->getEditable('unified_date.settings')
      ->set('node_types.publication', 'created')
      ->save();

    $midnight_three_days_ago = strtotime('midnight -3 days');
    $node->setCreatedTime($midnight_three_days_ago);
    $node->save();
    $output = $this->tokenService->replace('[node:unified_date]', ['node' => $node], ['langcode' => $this->interfaceLanguage->getId()], $bubbleable_metadata);
    $this->assertSame(date(DateFormat::load('fallback')->getPattern(), $midnight_three_days_ago), $output);

    // Change to use changed date.
    $config_factory = \Drupal::configFactory();
    $config_factory->getEditable('unified_date.settings')
      ->set('node_types.publication', 'changed')
      ->save();

    $midnight_two_days_ago = strtotime('midnight -2 days');
    $node->setChangedTime($midnight_two_days_ago);
    $node->save();
    $output = $this->tokenService->replace('[node:unified_date]', ['node' => $node], ['langcode' => $this->interfaceLanguage->getId()], $bubbleable_metadata);
    $this->assertSame(date(DateFormat::load('fallback')->getPattern(), $midnight_two_days_ago), $output);
  }

}
