<?php

namespace Drupal\Tests\unified_date\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\node\Entity\NodeType;
use Drupal\Tests\node\Traits\NodeCreationTrait;

/**
 * Unified date test base field existence.
 *
 * @group unified_date
 */
class UnifiedDateFieldTest extends KernelTestBase {

  use NodeCreationTrait;

  /**
   * The modules to enable for this test.
   *
   * @var array
   */
  protected static $modules = [
    'node',
    'field',
    'user',
    'text',
    'system',
    'datetime',
    'datetime_range',
    'unified_date',
    'unified_date_test_config',
  ];

  /**
   * The current database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The cron service.
   *
   * @var \Drupal\Core\Cron
   */
  protected $cron;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installSchema('node', ['node_access']);
    $this->installEntitySchema('user');
    $this->installEntitySchema('node');
    $this->installConfig(['field']);

    // Create article content type.
    NodeType::create([
      'type' => 'article',
      'name' => 'Article',
    ])->save();

    // Inject database connection, entity type manager and cron for the tests.
    $this->database = \Drupal::database();
    $this->entityTypeManager = \Drupal::entityTypeManager();
    $this->cron = \Drupal::service('cron');
  }

  /**
   * Test the base unified date field gets saved.
   */
  public function testBasefield(): void {
    $node = $this->createNode([
      'title' => 'Hello, world!',
      'type' => 'article',
    ]);
    $unified_date_value = $node->get('unified_date')->value;

    // By default, the unified_date field should match the created time.
    $this->assertNotEmpty($unified_date_value);
    $this->assertSame($unified_date_value, $node->getCreatedTime());
  }

  /**
   * Test the base unified date field gets saved.
   */
  public function testDateFields(): void {
    $config_factory = \Drupal::configFactory();
    $this->installConfig([
      'field',
      'node',
      'text',
      'datetime_range',
      'unified_date',
      'unified_date_test_config',
    ]);

    // Sample timestamps to test with.
    $midnight_today = strtotime('midnight');
    $one_day_from_now = strtotime('+1 day');
    $midnight_two_days_from_now = strtotime('midnight +2 days');
    $midnight_three_days_from_now = strtotime('midnight +3 days');

    $node = $this->createNode([
      'title' => 'Publication 1',
      'type' => 'publication',
    ]);
    $unified_date_value = $node->get('unified_date')->value;

    // By default, the unified_date field should match the created time.
    $this->assertNotEmpty($unified_date_value);
    $this->assertSame($unified_date_value, $node->getCreatedTime());

    // Now change the config to use field_publication_date.
    $config_factory = \Drupal::configFactory();
    $config_factory->getEditable('unified_date.settings')
      ->set('node_types.publication', 'field_publication_date')
      ->save();
    $node->set('field_publication_date', date('Y-m-d', $midnight_today));
    $node->save();
    $this->assertSame($node->get('unified_date')->value, $midnight_today);

    // Now change the config to use field_publication_date_and_time.
    $config_factory = \Drupal::configFactory();
    $config_factory->getEditable('unified_date.settings')
      ->set('node_types.publication', 'field_publication_date_and_time')
      ->save();
    $node->set('field_publication_date_and_time', date('Y-m-d\TH:i:s', $one_day_from_now));
    $node->save();
    $this->assertSame($node->get('unified_date')->value, $one_day_from_now);

    // Now change the config to use field_publication_date_and_time start date.
    $config_factory = \Drupal::configFactory();
    $config_factory->getEditable('unified_date.settings')
      ->set('node_types.publication', 'field_publication_date_range')
      ->save();
    $node->set('field_publication_date_range', [
      'value' => date('Y-m-d\TH:i:s', $midnight_two_days_from_now),
      'end_value' => date('Y-m-d\TH:i:s', $midnight_two_days_from_now),
    ]);
    $node->save();
    $this->assertSame($node->get('unified_date')->value, $midnight_two_days_from_now);

    // Now change the config to use field_publication_date_and_time end date.
    $config_factory = \Drupal::configFactory();
    $config_factory->getEditable('unified_date.settings')
      ->set('node_types.publication', 'field_publication_date_range:end_value')
      ->save();
    $node->set('field_publication_date_range', [
      'value' => date('Y-m-d\TH:i:s', $one_day_from_now),
      'end_value' => date('Y-m-d\TH:i:s', $midnight_three_days_from_now),
    ]);
    $node->save();
    $this->assertSame($node->get('unified_date')->value, $midnight_three_days_from_now);
  }

  /**
   * Test schema installs without issue.
   */
  public function testSchema() {
    $this->installConfig('unified_date');
  }

}
