This module is useful if you want to sort multiple content types in a unified list that have different date fields. For example:

1. Publications: Publication Date (added field)
2. Articles: Created Date (base field)
3. Events: End Date (added field)
4. Job vacancies: Application Deadline (added field)

You select which fields are the fields for the 'Unified Date' and this module syncs those fields into the new 'unified_date' base field that it creates. You can then for instance sort Views, Search, Recent Related Content, etc by that date.

## Features
Configure per node bundle which field should be stored in the Unified Date. If not set, it defaults to the Created date base field.
Use the batch form to run a batch script to auto-update content when you change the configuration

## Post-Installation
Go to the configuration page and decide on fields to use
Use the batch form to update existing content

## Supporting this Module
Initial development as well as ongoing maintenance is supported by <a href="https://designbysoapbox.com">Soapbox</a>.